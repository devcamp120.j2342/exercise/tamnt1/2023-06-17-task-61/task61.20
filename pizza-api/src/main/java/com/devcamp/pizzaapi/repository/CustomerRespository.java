package com.devcamp.pizzaapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.pizzaapi.models.Customer;

public interface CustomerRespository extends CrudRepository<Customer, Integer> {

}
