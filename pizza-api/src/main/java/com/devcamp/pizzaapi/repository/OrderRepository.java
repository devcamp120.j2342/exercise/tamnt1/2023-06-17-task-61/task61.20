package com.devcamp.pizzaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizzaapi.models.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

}
