package com.devcamp.pizzaapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.pizzaapi.models.Drink;
import com.devcamp.pizzaapi.repository.DrinkRepository;

@Service
public class DrinkService {
    private final DrinkRepository drinkRepository;

    public DrinkService(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    public void createDrink() {
        List<Drink> drinkList = new ArrayList<>();
        drinkList.add(new Drink("TRATAC", "Trà tắc", 10000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("COCA", "Cocacola", 15000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("PEPSI", "Pepsi", 15000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("LAVIE", "Lavie", 5000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("TRASUA", "Trà sữa trân châu", 40000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("FANTA", "Fanta", 15000, null, 1615177934000L, 1615177934000L));
        drinkRepository.saveAll(drinkList);
    }

    public List<Drink> getDrinkList() {
        return drinkRepository.findAll();
    }
}
