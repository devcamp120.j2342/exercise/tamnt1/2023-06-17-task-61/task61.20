package com.devcamp.pizzaapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.pizzaapi.models.Menu;
import com.devcamp.pizzaapi.repository.MenuRepository;

@Service
public class MenuService {
    private final MenuRepository menuRepository;

    public MenuService(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    public void createMenu() {
        List<Menu> menuList = new ArrayList<>();

        Menu small = new Menu("S (Small)", "20cm", 2, 200, 2, "150.000VND");
        Menu medium = new Menu("M (Medium)", "25cm", 4, 300, 3, "200.000VND");
        Menu large = new Menu("L (Large)", "30cm", 8, 500, 4, "250.000VND");
        menuList.add(small);
        menuList.add(medium);
        menuList.add(large);
        menuRepository.saveAll(menuList);
    }

    public List<Menu> getMenuList() {
        return menuRepository.findAll();
    }

}
