package com.devcamp.pizzaapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizzaapi.models.Drink;
import com.devcamp.pizzaapi.services.DrinkService;

@RestController
@CrossOrigin

public class DrinkController {

    @Autowired
    DrinkService drinkService;

    @GetMapping("/create-drink")
    public void createDrinkList() {
        drinkService.createDrink();
    }

    @GetMapping("/drink-list")
    public List<Drink> getDrinkList() {
        return drinkService.getDrinkList();
    }
}
