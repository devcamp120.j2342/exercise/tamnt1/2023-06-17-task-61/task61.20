package com.devcamp.pizzaapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizzaapi.models.Customer;
import com.devcamp.pizzaapi.services.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/customer-list")
    public List<Customer> getDrinkList() {
        return customerService.getCustomerList();
    }

    @GetMapping("/create-customer")
    public void creatDrink() {
        customerService.createCustomer();
    }

}
