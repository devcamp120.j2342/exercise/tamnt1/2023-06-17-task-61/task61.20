package com.devcamp.pizzaapi.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 30)
    private String size;
    @Column(length = 30)
    private String duongKinh;
    @Column(length = 30)
    private int suon;
    @Column(length = 30)
    private int salad;
    @Column(length = 30)
    private int nuoc;
    @Column(length = 30)
    private String thanhTien;

    public Menu() {
    }

    public Menu(String size, String duongKinh, int suon, int salad, int nuoc, String thanhTien) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.nuoc = nuoc;
        this.thanhTien = thanhTien;
    }

    public String getSize() {
        return size;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public int getSalad() {
        return salad;
    }

    public int getNuoc() {
        return nuoc;
    }

    public void setNuoc(int nuoc) {
        this.nuoc = nuoc;
    }

    public String getThanhTien() {
        return thanhTien;
    }

    @Override
    public String toString() {
        return "Menu [size=" + size + ", duongKinh=" + duongKinh + ", suon=" + suon + ", salad=" + salad
                + ", thanhTien=" + thanhTien + "]";
    }

}
