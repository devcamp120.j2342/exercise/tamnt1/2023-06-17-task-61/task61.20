package com.devcamp.pizzaapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizzaapi.models.Menu;
import com.devcamp.pizzaapi.services.MenuService;

@RestController
@CrossOrigin

public class MenuController {
    @Autowired
    MenuService menuService;

    @GetMapping("/create-menu")
    public void createMenu() {
        menuService.createMenu();
    }

    @GetMapping("/menu-list")
    public List<Menu> getMenu() {
        return menuService.getMenuList();
    }
}
