package com.devcamp.pizzaapi.controllers;

import java.util.List;

import org.aspectj.lang.annotation.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizzaapi.models.Order;
import com.devcamp.pizzaapi.services.OrderService;

@RestController
@CrossOrigin
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/order-list")
    public ResponseEntity<List<Order>> getOrder(
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size,
            @RequestParam(value = "sortField", defaultValue = "") String sortField,
            @RequestParam(value = "sortDir", defaultValue = "") String sortDir) {
        try {
            List<Order> orderList = orderService.getOrders(page, size, sortField, sortDir);
            return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace(); // Print the exception stack trace
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/create-orders")

    public void createOrder() {
        orderService.createOrders();
    }
}
