package com.devcamp.pizzaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizzaapi.models.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Integer> {

}
